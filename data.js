

const Sequelize = require('sequelize');
const dbConn = new Sequelize('users', ' root@localhost', '', {
  host: 'localhost',
  dialect: 'mysql',
  port:'3306',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
})
const sequelize = new Sequelize('mysql://root@localhost:3306/users', {
  // Look to the next section for possible options
})

dbConn
  .sync()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
    process.exit();
  });
 const User = sequelize.define('user', {
	ID:{
		type:Sequelize.STRING
	},

  FirstName: {
    type: Sequelize.STRING
  },
  LastName: {
    type: Sequelize.STRING
  },
  Mobile: {
  	type:  Sequelize.STRING
  },
  emailId: {
  	type: Sequelize.STRING
  }
});

// force: true will drop the table if it already exists
User.sync({force: true}).then(() => {
  // Table created
  return User.create({
  	ID:'1',
    FirstName: 'keerthi',
    lastName: 'Rajendran',
    Mobile:  '978695216',
    emailId:'kdkjekjwdknw@gmail.com'
  });
});