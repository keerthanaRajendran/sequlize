/**
 *  ------------------------------------------------------------------------
 *  
 *  Server Initialization
 *  
 * */

'use strict';

const express = require('express')
const app = express();
const request = require('request');
const http = require('http');
const APPPATH = require('app-root-path');
const entity = require(APPPATH + '/entity/index').sequelize.models;


// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {
  res.send('hello world')
});

app.get("/starwars/people", function(req, res, next){
	
	var searchURl = "https://swapi.co/api/people/?search=san";

	request(searchURl, function(error, response, html) {
		if (error) {
			console.log('Error:', error);
			res.send({
				data : "Error occured"
			});
		} else {
			var resData = JSON.parse(html);
			res.send({
				data : resData
			});
		}
	});
});

app.post('/insert', function (req, res) {

  	let sample =  entity.employee.build({
	    'first_name': 'Shanthi',
	    'last_name': 'Palani',
	    'mobile':  '8056593267',
	    'email_id':'shanthipalani93@gmail.com'
	  	});

	 sample.save();

    res.send('Data inserted successfully');
});


app.get('/get_user', function(req, res){

    let id = req.query.id;
   entity.employee.findAll({
     where : { 'id': id}
   })
   .then(function(user){
      res.send(user);
   })
})
app.post('/update',function(req,res){

  entity.employee.update({'first_name':'keerthu'},{ 
    where: {'id':1 }
})
.then((success) => {
    return success;
})
.catch((error) => {
    return new Error(error);
})
res.send('Data updated successfully');
})
app.listen(3000);
