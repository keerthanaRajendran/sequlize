/**
 *  ------------------------------------------------------------------------
 *  
 *  PM2 related configuration
 *  
 **/

module.exports = {
  apps : [{
    name: 'sample_app',
    script: './index.js',
    log_date_format: 'YYYY-MM-DD HH:mm Z',
    error_file: './logs/err.log',
    out_file: './logs/out.log'
  }]
}
