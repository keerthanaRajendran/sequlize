/**
 *  ------------------------------------------------------------------------
 *  
 *  Database connection initialization
 *  
 * */

'use strict';

const fs          = require('fs');
const path        = require('path');
const APPPATH = require('app-root-path');
const config      = require(APPPATH +'/config');
const Sequelize   = require('sequelize');
const basename    = path.basename(module.filename);


const sequelize = new Sequelize(config.dbConfig.DB, config.dbConfig.USERNAME, config.dbConfig.PASSWORD, {
    host        : config.dbConfig.HOST,
    dialect     : 'mysql',
    logging     : true,
    omitNull    : false,
    native      : true,
    language    : 'en',
    pool        : {
        max: 5,
        min: 0,
        idle: 30000,
        acquire: 20000
    }
});

sequelize
.sync()
.then(function(){
    console.log('DB connection sucessful.');
})
.catch(function(err){
    console.log('DB connection failed..',err);
    process.exit();
});

const db = {};
fs.readdirSync(APPPATH + '/entity').filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
}).forEach(function(file) {
    const model = sequelize.import(path.join(APPPATH + '/entity', file));
    db[model.name] = model;
});

Object.keys(db).forEach(function(modelName) {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;

module.exports = db;