/**
 *  ------------------------------------------------------------------------
 *  
 *  Table Definition - Employee
 *  
 * */

'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const employee = sequelize.define('employee', {
        id : { type: Sequelize.INTEGER, autoIncrement: true,primaryKey: true},
        first_name : Sequelize.STRING,
        last_name : Sequelize.STRING,
        mobile : Sequelize.STRING,
        email_id : Sequelize.STRING    
    },
    {
        timestamps: false,  
        tableName: 'employee'
    });

    employee.associate = (models) => {
                
    }

    employee.sync({force: false});

    return employee;

}